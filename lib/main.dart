import 'package:flutter/material.dart';
import 'package:medicine_warehouse/Controllers/medicine_controller.dart';
import 'package:medicine_warehouse/Controllers/navigation_controller.dart';
import 'package:medicine_warehouse/Controllers/orders_controller.dart';
import 'package:medicine_warehouse/Controllers/reports_controller.dart';
import 'package:medicine_warehouse/Controllers/theme_controller.dart';
import 'package:medicine_warehouse/Localization/localization.dart';
import 'package:medicine_warehouse/Localization/localization_controller.dart';
import 'package:medicine_warehouse/Screens/home_screen.dart';
import 'package:medicine_warehouse/Screens/login_screen.dart';
import 'package:medicine_warehouse/Controllers/search_controller.dart';
import 'package:medicine_warehouse/Screens/reports_screen.dart';
//import 'package:medicine_warehouse/Screens/register_screen.dart';
import 'package:get/get.dart';
import 'package:medicine_warehouse/Screens/register_screen.dart';
import 'package:medicine_warehouse/main.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  Get.put(NavigationController());
  //Get.put(MedicineController());
  Get.put( MedicineController());
  Get.put(SearchControl());
  Get.put(OrdersController());
  MyLocaleController myLocaleController = Get.put(MyLocaleController());
  Get.put(ReportsController());
  runApp(WareHouse());
}

class WareHouse extends StatelessWidget {
  final ThemeController themeController = Get.put(ThemeController());

  WareHouse({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => GetMaterialApp(locale:Locale('en'),
        translations: MyLocale(),
        debugShowCheckedModeBanner: false,
        theme: themeController.themeData,
        home: const LoginScreen(),
      ),
    );
  }
}
