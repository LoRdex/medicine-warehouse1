import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medicine_warehouse/Controllers/login_controller.dart';
import 'package:medicine_warehouse/Controllers/medicine_controller.dart';
import 'package:medicine_warehouse/Controllers/search_controller.dart';
import 'package:medicine_warehouse/Controllers/theme_controller.dart';
import 'package:medicine_warehouse/Localization/localization_controller.dart';
import 'package:medicine_warehouse/Screens/orders_screen.dart';
import 'package:medicine_warehouse/Screens/home_content_screen.dart';
import 'package:medicine_warehouse/Screens/items_add_screen.dart';
import 'package:medicine_warehouse/Controllers/navigation_controller.dart';
import 'package:medicine_warehouse/Screens/reports_screen.dart';
import 'package:medicine_warehouse/Screens/search_screen.dart';



class HomeScreen extends StatelessWidget {
  MyLocaleController myLocaleController = Get.find();
  MedicineController medicineController = Get.find();
  LoginController loginController = Get.find();
  final ThemeController themeController = Get.find();
  NavigationController navigationController = Get.find();
  SearchControl searchControl = Get.find();


  final List<Widget> screens = [
    const HomeScreenContent(),
    const AddScreen(),
    const OrdersScreen(),
     ReportsScreen()
  ];

  HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            'PharmaME',
            style:  TextStyle(fontFamily: "Besley", fontSize: 30),
          ),
          bottom: TabBar(
            onTap: (index) {
              navigationController.changeTabIndex(index);
            },
            tabs:  [
              Tab(text: '25'.tr),
              Tab(text: '26'.tr),
              Tab(text: '27'.tr),
              Tab(text: '87'.tr,)
            ],
          ),
        ),
        body: Row(
          children: [
            Obx(
                  () => NavigationRail(
                selectedIndex: navigationController.currentNavIndex.value,
                onDestinationSelected: (int index) {
                  navigationController.changeNavIndex(index);
                },
                labelType: NavigationRailLabelType.selected,
                destinations: [
                  NavigationRailDestination(
                    padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height/24),
                    icon: IconButton(
                      icon: const Icon(Icons.refresh),
                      onPressed: () {
                        medicineController.getMedicines();
                        ordersController.getOrders();

                      },
                    ),
                    label:  Text('28'.tr),
                  ),
                  NavigationRailDestination(
                    padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height/24),
                    icon: IconButton(
                      icon: const Icon(Icons.search),
                      onPressed: () {
                      Get.to(SearchScreen());
                        // Add your action for the search icon here
                      },
                    ),
                    label:  Text('29'.tr),
                  ),
                  NavigationRailDestination(
                    padding: EdgeInsets.symmetric(vertical : MediaQuery.of(context).size.height/24),
                    icon: IconButton(
                      icon: themeController.isDarkTheme.value
                          ? const Icon(Icons.sunny,)
                          : const Icon(Icons.dark_mode),
                      onPressed: () {
                        themeController.switchTheme();
                      },
                    ), label:  Text('30'.tr),
                  ),
                  NavigationRailDestination(
                    padding: EdgeInsets.symmetric(vertical :MediaQuery.of(context).size.height/24),

                    icon: IconButton(
                      icon: const Icon(Icons.language),
                      onPressed: () {
                        myLocaleController.changeLang();
                      },
                    ),
                    label:   Text('31'.tr,),
                  ),
                  NavigationRailDestination(
                    padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height/24),

                    icon: IconButton(
                      icon: const Icon(Icons.door_back_door),
                      onPressed: () {
                       Get.dialog(AlertDialog(
                         title:   Text('33'.tr),
                         content: SizedBox(
                           height: MediaQuery.of(context).size.height/7,
                           child: Column(
                             children: [
                               Center(
                                 child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                   children: [
                                     TextButton(onPressed: (){loginController.logout();
                                    // Get.reset();

                                     }, child:  Text("34".tr)),
                                     TextButton(onPressed: (){Get.back();}, child:   Text("35".tr)),
                                   ],
                                 ),
                               )
                             ],
                           ),
                         ),
                       ));

                        // Add your action for the door icon here
                      },
                    ),
                    label:   Text('32'.tr),
                  ),


                ],
              ),
            ),
            const VerticalDivider(thickness: 2, width: 1),
            Expanded(
              child: Obx(() => screens[navigationController.currentTabIndex.value]),
            ),
          ],
        ),
      ),
    );
  }
}

