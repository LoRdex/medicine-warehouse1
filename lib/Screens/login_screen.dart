import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:medicine_warehouse/Components/colors.dart';
import 'package:medicine_warehouse/Components/textfiled.dart';
import 'package:medicine_warehouse/Controllers/login_controller.dart';
import 'package:medicine_warehouse/Controllers/navigation_controller.dart';
import 'package:medicine_warehouse/Controllers/reports_controller.dart';
import 'package:medicine_warehouse/Controllers/theme_controller.dart';
import 'package:medicine_warehouse/Localization/localization_controller.dart';
import 'package:medicine_warehouse/Screens/register_screen.dart';
import 'package:get/get.dart';
MyLocaleController myLocaleController = Get.find();
LoginController loginController = Get.put(LoginController());
final ThemeController themeController = Get.find();
NavigationController navigationController = Get.find();
ReportsController reportsController = Get.find();

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  const Text(
          'PharmaMe', style:  TextStyle(fontFamily: 'Besley', fontSize: 30,color: MyColors.midpurple),),
        actions: [  ElevatedButton(
          onPressed: () {
    myLocaleController.changeLang();
    },
        child:
              Text("31".tr)
        ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: ElevatedButton(
                onPressed: () {
                  themeController.switchTheme();
                },
                child: Obx(
                      () =>
                      Text(themeController.isDarkTheme.value
                          ? '79'.tr
                          : '78'.tr),
                )),
          )
        ],
      ),
      body: Row(
        children: [
        Expanded(
        flex: 1,
        child: Column(
          children: [
            Expanded(
              child: SizedBox(
                width: MediaQuery.of(context).size.width,

                child: Lottie.asset(
                  'lotties/register.json',
                ),
              ),
            ),
          ],
        ),
      ),
      Expanded(
        flex: 1,
        child: Center(
          child: Form(
              key: loginController.formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment
                    .center, // This will center the column
                children: [
                const Text(
                'PharmaME',
                style: TextStyle(
                    fontSize: 50,
                    fontFamily: 'Besley',
                    color: MyColors.midpurple2,
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 60),
              CustomTextFormField(
                labelText: '1'.tr,
                controller: loginController.emailController,
                keyType: TextInputType.emailAddress,
                preIcon: Icons.alternate_email,
                validator: loginController.emailOrPhoneValidator,)
                  ,
          const SizedBox(height: 60),
          Obx(() =>
              CustomTextFormField(
                controller: loginController.passwordController,
                validator: loginController.passwordValidator,
                labelText: "2".tr,
                obscureText: loginController.passwordVisibility.value,
                preIcon: Icons.password,
                suffixIcon: IconButton(
                  onPressed: () {
                    loginController.setPasswordVisibility();
                  },
                  icon: Icon(
                    loginController.passwordVisibility.value
                        ? Icons.visibility_off
                        : Icons.visibility,
                  ),
                ),
                keyType: TextInputType.text,
              ),
          ),
          const SizedBox(
            height: 40,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: InkWell(
              onTap: () {
                if (loginController.formKey.currentState!
                    .validate()) {}

                // Login Post Request write it here . //
                 loginController.login(
                     loginController.emailController.text,
                   loginController.passwordController.text);
              },
              child: Container(
                width: 250,
                padding: const EdgeInsets.symmetric(
                    vertical: 10.0, horizontal: 20.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30.0),
                  color: MyColors.midpurple2

                ),
                child:  Center(
                  child: Text(maxLines: 1,
                    "3".tr,
                    style: const TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontFamily: 'Noto_Sans_Tai_Viet'),
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
               Text("4".tr),
              TextButton(
                onPressed: () {
                  Get.off(() => const RegisterScreen());
                },
                child: Text(
                  '5'.tr,
                  style: const  TextStyle(color: MyColors.midpurple2),
                ),
              ),
            ],
          )
          ],
        ),
                ),
              ),)
    ,
    ]
    ,
    )
    ,
    );
  }
}
