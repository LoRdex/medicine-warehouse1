import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:medicine_warehouse/Components/colors.dart';
import 'package:medicine_warehouse/Controllers/orders_controller.dart';



class OrdersScreen extends StatelessWidget {
  const OrdersScreen({super.key});

  @override
  Widget build(BuildContext context) {
    OrdersController ordersController = Get.find<OrdersController>();



    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 30),
      child: ListView.builder(
        itemCount: ordersController.orders.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 2,
                height: MediaQuery.of(context).size.height / 6,
                decoration: BoxDecoration(
                    color: MyColors.midpurple2,
                    boxShadow: [
                      BoxShadow(
                        color: MyColors.midpurple.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 7,
                        offset: const Offset(0, 3),
                      ),
                    ],
                    borderRadius: BorderRadius.circular(25)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      "${'100'.tr} : ${ordersController.orders[index].id}",
                      style: const TextStyle(color: MyColors.lightpurple2),
                    ),
                    TextButton(
                        onPressed: () async {
                          await ordersController.getOrderDetails(
                              ordersController.orders[index].id);
                          Get.dialog(AlertDialog(
                            title:  Text("49".tr),
                            content: Column(
                              children: [
                                Text(
                                    " ${'91'.tr}: ${ordersController.details[0].status}",),
                                const SizedBox(
                                  height: 20,
                                ),
                                Text(
                                    " ${'92'.tr}: ${ordersController.details[0].payStatus}"),
                                const SizedBox(
                                  height: 20,
                                ),
                                ...ordersController.details[0].medicines
                                        ?.map((medicine) {
                                      return Column(
                                        children: [
                                          Text(
                                              " ${'93'.tr} : ${medicine.scName}"),
                                          Text(
                                              " ${'43'.tr} : ${medicine.quantity}"),
                                          const Text(
                                              " ________________________"),
                                        ],
                                      );
                                    }).toList() ??
                                    [],
                                Text(
                                    '${'94'.tr}: ${ordersController.details[0].totalPrice}',style: const TextStyle(color: MyColors.purple),),
                              ],
                            ),
                            actions: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  TextButton(
                                      onPressed: () {
                                        Get.back();
                                      },
                                      child: Text("50".tr))
                                ],
                              )
                            ],
                          ));
                        },
                        child: Text("49".tr,
                            style: const TextStyle(color: MyColors.lightpurple2))),
                    TextButton(
                        onPressed: () {
                          Get.dialog(AlertDialog(
                            title: Text("68".tr),
                            content: SizedBox(
                              height: MediaQuery.of(context).size.height / 3,
                              child: Column(
                                children: [
                                  Text("69".tr),
                                  Obx(() => DropdownButton<String>(
                                        value: ordersController
                                            .selectedStatus.value,
                                        onChanged: (String? newValue) {
                                          if (newValue != null) {
                                            ordersController
                                                .updateStatus(newValue);
                                          }
                                        },
                                        items: ordersController.status
                                            .map<DropdownMenuItem<String>>(
                                                (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(value),
                                          );
                                        }).toList(),
                                      )),
                                  Text("70".tr),
                                  Obx(() => DropdownButton<String>(
                                        value: ordersController
                                            .selectedPaymentStatus.value,
                                        onChanged: (String? newValue) {
                                          if (newValue != null) {
                                            ordersController
                                                .updatePaymentStatus(newValue);
                                          }
                                        },
                                        items: ordersController.paymentStatus
                                            .map<DropdownMenuItem<String>>(
                                                (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(value),
                                          );
                                        }).toList(),
                                      )),
                                ],
                              ),
                            ),
                            actions: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  TextButton(
                                      onPressed: () {
                                        ordersController.editStatus(
                                            ordersController
                                                .selectedStatus.value,
                                            ordersController.orders[index].id);
                                        ordersController.editPaymentStatus(
                                            ordersController
                                                .selectedPaymentStatus.value,
                                            ordersController.orders[index].id);
                                        ordersController.getOrders();
                                        Get.back();
                                      },
                                      child: Text("71".tr)),
                                  TextButton(
                                      onPressed: () {
                                        Get.back();
                                      },
                                      child: Text("50".tr)),
                                ],
                              )
                            ],
                          ));
                        },
                        child: Text(
                          "72".tr,
                          style: const TextStyle(color: MyColors.lightpurple2),
                        )),
                  ],
                ),
              ),
              const SizedBox(
                height: 50,
              )
            ],
          );
        },
      ),
    );
  }
}
