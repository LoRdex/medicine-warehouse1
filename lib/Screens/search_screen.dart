import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medicine_warehouse/Components/colors.dart';
import 'package:medicine_warehouse/Components/textfiled.dart';
import 'package:medicine_warehouse/Controllers/login_controller.dart';
import 'package:medicine_warehouse/Controllers/medicine_controller.dart';
import 'package:medicine_warehouse/Controllers/navigation_controller.dart';
import 'package:medicine_warehouse/Controllers/search_controller.dart';
import 'package:medicine_warehouse/Controllers/theme_controller.dart';
import 'package:medicine_warehouse/Localization/localization_controller.dart';

class SearchScreen extends StatelessWidget {
  SearchScreen({Key? key}) : super(key: key);

  SearchControl searchControl = Get.find();
  LoginController loginController = Get.find();
  NavigationController navigationController = Get.find();
  MedicineController medicineController = Get.find();
  ThemeController themeController = Get.find();
  MyLocaleController myLocaleController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: const Text(
          'PharmaME',
          style: TextStyle(fontFamily: "Besley", fontSize: 30),
        ),
      ),
      body: Row(
        children: [
          Obx(
            () => NavigationRail(
              selectedIndex: navigationController.currentNavIndex.value,
              onDestinationSelected: (int index) {
                navigationController.changeNavIndex(index);
              },
              labelType: NavigationRailLabelType.selected,
              destinations: [
                NavigationRailDestination(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 24),
                  icon: IconButton(
                    icon: const Icon(Icons.arrow_back),
                    onPressed: () {
                      // Add your action for the language icon here
                      Get.back();
                    },
                  ),
                  label: Text('47'.tr),
                ),
                NavigationRailDestination(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 24),
                  icon: IconButton(
                    icon: themeController.isDarkTheme.value
                        ? const Icon(
                            Icons.sunny,
                          )
                        : const Icon(Icons.dark_mode),
                    onPressed: () {
                      themeController.switchTheme();
                    },
                  ),
                  label: Text('30'.tr),
                ),
                NavigationRailDestination(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 24),
                  icon: IconButton(
                    icon: const Icon(Icons.language),
                    onPressed: () {
                      myLocaleController.changeLang();
                    },
                  ),
                  label: Text('31'.tr),
                ),
                NavigationRailDestination(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 24),
                  icon: IconButton(
                    icon: const Icon(Icons.door_back_door),
                    onPressed: () {
                      Get.dialog(AlertDialog(
                        title: Text('33'.tr),
                        content: Container(
                          height: MediaQuery.of(context).size.height / 5,
                          child: Column(
                            children: [
                              Center(
                                child: Row(
                                  children: [
                                    TextButton(
                                        onPressed: () {
                                          loginController.logout();
                                        },
                                        child: Text("34".tr)),
                                    TextButton(
                                        onPressed: () {
                                          Get.back();
                                        },
                                        child: Text("35".tr)),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ));
                    },
                  ),
                  label: Text('32'.tr),
                ),
              ],
            ),
          ),
          const VerticalDivider(
            thickness: 1,
            width: 1,
          ),
          Expanded(
            child: Center(
              child: Form(
                key: searchControl.searchKey,
                child: Column(
                  children: [
                    CustomTextFormField(
                        labelText: "48".tr,
                        controller: searchControl.searchTextController,
                        suffixIcon: IconButton(
                            onPressed: () {
                              if (searchControl.searchKey.currentState!
                                  .validate()) {}
                              searchControl.results.clear();
                              searchControl.search(
                                  searchControl.searchTextController.text);
                            },
                            icon: const Icon(Icons.search)),
                        validator: searchControl.searchValidator),
                    Obx(() => GridView.count(
                          shrinkWrap: true,
                          crossAxisCount: 4,
                          mainAxisSpacing: 1.0,
                          crossAxisSpacing: 1.0,
                          childAspectRatio: 1 / .65,
                          children: List.generate(
                              searchControl.results.length,
                              (index) => Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Container(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              3,
                                      width:
                                          MediaQuery.of(context).size.width / 5,
                                      decoration: BoxDecoration(
                                        color: MyColors.midpurple,
                                        borderRadius: BorderRadius.circular(18),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.4),
                                            spreadRadius: 1,
                                            blurRadius: 7,
                                            offset: const Offset(0,
                                                3), // changes position of shadow
                                          ),
                                        ],
                                      ),
                                      child: GestureDetector(
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              top: 15.0, left: 15.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Center(
                                                child: CircleAvatar(
                                                  backgroundColor: Colors.white,
                                                  radius: MediaQuery.of(context)
                                                          .size
                                                          .width /
                                                      50,
                                                  child: Image.asset(
                                                    '/images/${searchControl.results[index].image}',
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            40,
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height /
                                                    100,
                                              ),
                                              Center(
                                                child: Text(
                                                  '${searchControl.results[index].trName}',
                                                  style: const TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.w900,
                                                      color: Colors.white),
                                                ),
                                              ),
                                              SizedBox(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height /
                                                    100,
                                              ),
                                              Row(
                                                children: [
                                                  Text(
                                                    '${searchControl.results[index].category}',
                                                    style: const TextStyle(
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        color: Colors.grey),
                                                  ),
                                                  const Spacer(),
                                                  Text(
                                                    ' ${searchControl.results[index].quantity}',
                                                    style: const TextStyle(
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        color: Colors.grey),
                                                  ),
                                                  SizedBox(
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width /
                                                              100),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        onTap: () {
                                          Get.dialog(AlertDialog(
                                            title: Text("49".tr),
                                            content: Container(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height /
                                                  2,
                                              child: Column(
                                                children: [
                                                  Text(
                                                      "Trade Name : ${searchControl.results[index].trName}"),
                                                  SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height /
                                                            30,
                                                  ),
                                                  Text(
                                                      "Scientific Name : ${searchControl.results[index].scName}"),
                                                  SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height /
                                                            30,
                                                  ),
                                                  Text(
                                                      "Manufacture : ${searchControl.results[index].manufacture}"),
                                                  SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height /
                                                            30,
                                                  ),
                                                  Text(
                                                      "Category : ${searchControl.results[index].category}"),
                                                  SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height /
                                                            30,
                                                  ),
                                                  Text(
                                                      "Quantity : ${searchControl.results[index].quantity}"),
                                                  SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height /
                                                            30,
                                                  ),
                                                  Text(
                                                      "Expiration Date : ${searchControl.results[index].expDate}"),
                                                  SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height /
                                                            30,
                                                  ),
                                                  Text(
                                                      "Medicine Price : ${searchControl.results[index].price}"),
                                                ],
                                              ),
                                            ),
                                            actions: <Widget>[
                                              Center(
                                                child: TextButton(
                                                  child: Text('50'.tr),
                                                  onPressed: () {
                                                    Get.back();
                                                  },
                                                ),
                                              ),
                                            ],
                                          ));
                                        },
                                      ),
                                    ),
                                  )),
                        ))
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
