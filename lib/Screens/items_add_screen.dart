import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medicine_warehouse/Components/textfiled.dart';
import 'package:medicine_warehouse/Controllers/items_add_controller.dart';
import 'package:medicine_warehouse/Controllers/navigation_controller.dart';

AddController addController = Get.put(AddController());
NavigationController navigationController = Get.find();


class AddScreen extends StatelessWidget {
  const AddScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Form(
      key: addController.addFormKey,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            CustomTextFormField(
              labelText: "36".tr,
              controller: addController.trNameController,
              validator: addController.trNameValidator,
              keyType: TextInputType.name,
            ),
            CustomTextFormField(
              labelText: "37".tr,
              controller: addController.scNameController,
              validator: addController.scNameValidator,
              keyType: TextInputType.name,
            ),
            CustomTextFormField(
              labelText: "38".tr,
              controller: addController.manufactureController,
              validator: addController.manufactureValidator,
              keyType: TextInputType.name,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width / 2.5,
              child: Obx(() {
                return DropdownButtonFormField<int>(
                  value: addController.selectedCategoryIndex.value + 1,
                  onChanged: (int? newValue) {
                    addController.updateCategory(newValue! - 1);
                    log(addController.selectedCategoryIndex.value);
                  },
                  items: addController.categories
                      .asMap()
                      .entries
                      .map<DropdownMenuItem<int>>((entry) {
                    int index = entry.key;
                    String value = entry.value;
                    return DropdownMenuItem<int>(
                      value: index + 1,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 50),
                        child: Text(value),
                      ),
                    );
                  }).toList(),
                  validator: (int? value) {
                    if (value == null || value == 0) {
                      return "Select a Medicine Category";
                    }
                  },
                );
              }),
            ),
            CustomTextFormField(
              labelText: "43".tr,
              controller: addController.quantityController,
              validator: addController.quantityValidator,
              keyType: TextInputType.number,
            ),
            CustomTextFormField(
              labelText: "44".tr,
              controller: addController.expDateController,
              validator: addController.expDateValidator,
              keyType: TextInputType.datetime,
              onTap: () {
                addController.selectExpiryDate(context);
              },
            ),
            CustomTextFormField(
              labelText: "45".tr,
              controller: addController.priceController,
              validator: addController.priceValidator,
              keyType: TextInputType.number,
            ),
            ElevatedButton(
                onPressed: () {
                  if (addController.addFormKey.currentState!.validate()) {}
                  String expDate = addController.expDateController.text;
                  String categoryId =
                      (addController.selectedCategoryIndex.value + 1)
                          .toString();
                  addController.add(
                      addController.scNameController.text,
                      addController.trNameController.text,
                      addController.manufactureController.text,
                      categoryId,
                      expDate,
                      addController.priceController.text,
                      addController.quantityController.text);
                },
                child:  Text('46'.tr))
          ],
        ),
      ),
    );
  }
}
