import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medicine_warehouse/Components/colors.dart';
import 'package:medicine_warehouse/Controllers/medicine_controller.dart';
import 'package:medicine_warehouse/Controllers/navigation_controller.dart';
import 'package:medicine_warehouse/Controllers/search_controller.dart';

final medicineController = Get.find<MedicineController>();
NavigationController navigationController = Get.find();
SearchControl searchControl = Get.find();

class HomeScreenContent extends StatelessWidget {
  const HomeScreenContent({super.key});

  @override
  Widget build(BuildContext context) {
   
    return Obx(() => medicineController.medicines.isEmpty ? Center(
      child: Container(
        child:  Text("80".tr,style: TextStyle(fontSize: 40),),
      ),
    )
    : GridView.count(
      shrinkWrap: true,
      crossAxisCount: 4,
      mainAxisSpacing: 1.0,
      crossAxisSpacing: 1.0,
      childAspectRatio: 1 / .65,
      children: List.generate(
          medicineController.medicines.length,
              (index) => Padding(
            padding: const EdgeInsets.all(16.0),
            child: Container(
              height: MediaQuery.of(context).size.height / 3,
              width: MediaQuery.of(context).size.width / 5,
              decoration: BoxDecoration(
                color: MyColors.midpurple,
                borderRadius: BorderRadius.circular(18),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 7,
                    offset: const Offset(
                        0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: GestureDetector(
                child: Padding(
                  padding:
                  const EdgeInsets.only(top: 15.0, left: 15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                        child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius:
                          MediaQuery.of(context).size.width / 50,
                          child: Image.asset(
                            '/images/${medicineController.medicines[index].image}',
                            width:
                            MediaQuery.of(context).size.width / 40,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(
                        height:
                        MediaQuery.of(context).size.height / 100,
                      ),
                      Center(
                        child: Text(
                          '${medicineController.medicines[index].trName}',
                          style: const TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w900,
                              color: Colors.white),
                        ),
                      ),
                      SizedBox(
                        height:
                        MediaQuery.of(context).size.height / 100,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Row(
                          children: [
                            Text(
                              '${medicineController.medicines[index].category}',
                              style: const TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.grey),
                            ),
                            const Spacer(),
                            Text(
                              ' ${medicineController.medicines[index].quantity}',
                              style: const TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.grey),
                            ),
                            SizedBox(
                                width:
                                MediaQuery.of(context).size.width /100
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  Get.dialog(AlertDialog(
                    title: Text("${'49'.tr}:"),
                    content: Container(
                      height: MediaQuery.of(context).size.height/2,
                      child: Column(
                        children: [
                          Text(
                              "${'36'.tr} : ${medicineController.medicines[index].trName}"),
                          SizedBox(
                            height:
                            MediaQuery.of(context).size.height /
                                30,
                          ),
                          Text(
                              "${'37'.tr} : ${medicineController.medicines[index].scName}"),
                          SizedBox(
                            height:
                            MediaQuery.of(context).size.height /
                                30,
                          ),
                          Text(
                              "${'38'.tr} : ${medicineController.medicines[index].manufacture}"),
                          SizedBox(
                            height:
                            MediaQuery.of(context).size.height /
                                30,
                          ),
                          Text(
                              "${'90'.tr}: ${medicineController.medicines[index].category}"),
                          SizedBox(
                            height:
                            MediaQuery.of(context).size.height /
                                30,
                          ),
                          Text(
                              "${'43'.tr}: ${medicineController.medicines[index].quantity}"),
                          SizedBox(
                            height:
                            MediaQuery.of(context).size.height /
                                30,
                          ),
                          Text(
                              "${'44'.tr}: ${medicineController.medicines[index].expDate}"),
                          SizedBox(
                            height:
                            MediaQuery.of(context).size.height /
                                30,
                          ),
                          Text(
                              "${'45'.tr} : ${medicineController.medicines[index].price}"),
                        ],
                      ),
                    ),
                    actions: <Widget>[
                      Center(
                        child: TextButton(
                          child:  Text('50'.tr),
                          onPressed: () {
                            Get.back();
                          },
                        ),
                      ),
                    ],
                  ));
                },
              ),
            ),
          )),
    ));
  }
}
