import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:medicine_warehouse/Components/colors.dart';
import 'package:medicine_warehouse/Controllers/navigation_controller.dart';
import 'package:medicine_warehouse/Controllers/register_controller.dart';
import 'package:medicine_warehouse/Controllers/theme_controller.dart';
import 'package:medicine_warehouse/Localization/localization_controller.dart';
import 'package:medicine_warehouse/Screens/login_screen.dart';
import 'package:medicine_warehouse/Components/textfiled.dart';
import 'package:get/get.dart';

MyLocaleController myLocaleController = Get.find();
RegisterController registerController = Get.put(RegisterController());
 ThemeController themeController = Get.find();
NavigationController navigationController = Get.find();

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('PharmaME',style: TextStyle(color: MyColors.midpurple,),),
        actions: [ElevatedButton(
          onPressed: () {
    myLocaleController.changeLang();
    },
        child:
        Text("31".tr)
    ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: ElevatedButton(
                onPressed: () {
                  themeController.switchTheme();
                },
                child: Obx(
                      () => Text(themeController.isDarkTheme.value
                      ? '79'.tr
                      : '78'.tr),
                )),
          )
        ],
      ),
      body: Row(
        children: [
          Obx(
            () => Expanded(
              flex: 1,
              child: Center(
                child: Form(
                  key: registerController.formKeyReg,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'PharmaME',
                        style: TextStyle(
                          fontSize: 40,
                          fontFamily: 'Besley',
                          color: MyColors.midpurple2,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                     //  SizedBox(height: MediaQuery.of(context).size.height/12),
                      CustomTextFormField(
                        controller: registerController.nameController,
                        validator: registerController.nameValidator,
                        labelText: "6".tr,
                        preIcon: Icons.accessibility_rounded,
                        keyType: TextInputType.text,
                      ),
                       SizedBox(height: MediaQuery.of(context).size.height/17),
                      CustomTextFormField(
                        controller: registerController.phoneController,
                        validator: registerController.phoneValidator,
                        labelText: "7".tr,
                        preIcon: Icons.phone,
                        keyType: TextInputType.text,
                      ),
                       SizedBox(
                        height:  MediaQuery.of(context).size.height/17,
                      ),
                      CustomTextFormField(
                        controller: registerController.emailController,
                        validator: registerController.emailValidator,
                        labelText: "8".tr,
                        preIcon: Icons.alternate_email,
                        keyType: TextInputType.text,
                      ),
                       SizedBox(
                        height:  MediaQuery.of(context).size.height/17,
                      ),
                      CustomTextFormField(
                        controller: registerController.passwordController,
                        validator: registerController.passwordValidator,
                        labelText: "9".tr,
                        obscureText:
                            registerController.passwordVisibility.value,
                        preIcon: Icons.password,
                        suffixIcon: IconButton(
                          onPressed: () {
                            registerController.setPasswordVisibility();
                          },
                          icon: Icon(
                            registerController.passwordVisibility.value
                                ? Icons.visibility_off
                                : Icons.visibility,
                          ),
                        ),
                        keyType: TextInputType.text,
                      ),
                       SizedBox(
                        height:  MediaQuery.of(context).size.height/17,
                      ),
                      //

                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: InkWell(
                          onTap: () {
                            if (registerController.formKeyReg.currentState!
                                .validate()) {}
                            registerController.register(

                                registerController.emailController.text,
                                registerController.passwordController.text,
                                registerController.nameController.text,
                                registerController.phoneController.text);

                            // register post operation should be written here :) //
                          },
                          child: Container(
                            width: 150,
                            padding: const EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 20.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30.0),
                              color: MyColors.midpurple2
                            ),
                            child:  Center(
                              child: Text(
                                "10".tr,
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontFamily: 'Besley'),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                           Text("11".tr),
                          TextButton(
                            onPressed: () {
                              Get.off(() => const LoginScreen());
                            },
                            child:  Text(
                              '12'.tr,
                              style:const  TextStyle(color: MyColors.midpurple2),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: SizedBox(
              width: MediaQuery.of(context).size.width,

              child: Lottie.asset(
                'lotties/lab2.json',
              ),
            ),
          ),
        ],
      ),
    );
  }
}
