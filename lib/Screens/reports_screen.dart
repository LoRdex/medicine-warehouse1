
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medicine_warehouse/Components/colors.dart';
import 'package:medicine_warehouse/Controllers/reports_controller.dart';


class ReportsScreen extends StatelessWidget {
  ReportsController reportsController = Get.find();
   ReportsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return

      reportsController.reportsModel == null ? Scaffold(body: Center(child: Text("89".tr,style: const TextStyle(fontSize: 50),)),)
      :Scaffold(
      body:Padding(
        padding: const EdgeInsets.only(bottom: 200),
        child: Center(
          child: Container(
            width:  MediaQuery.of(context).size.width / 2,
            height: MediaQuery.of(context).size.height / 6,
            decoration: BoxDecoration(
                color: MyColors.midpurple2,
                boxShadow: [
                  BoxShadow(
                    color: MyColors.midpurple.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 7,
                    offset: const Offset(0, 3),
                  ),
                ],
                borderRadius: BorderRadius.circular(25)),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextButton(
                  onPressed: (){
                    Get.dialog(
                        AlertDialog(
                          title: Text("${'95'.tr}:"),
                          content: Column(
                            children: [
                              ...reportsController.reports.map((report) {
                                return Column(
                                  children: [
                                    Text("${'96'.tr}: ${report.trName}"),
                                    Text(" ${'97'.tr} : ${report.totalQuantity}"),
                                    Text(" ${'98'.tr}  : ${report.totalPrice}"),
                                    const Text("____________________________")
                                  ],
                                );

                              }).toList(),
                              Text("${'99'.tr} :${reportsController.reportsModel!.totalSales} "),
                          // Add this line
                            ],
                          ),
                          actions: [
                            Center(child: TextButton(onPressed: (){Get.back();}, child: Text("50".tr)))
                          ],
                        )
                    );

                  },
                  child:  Center(
                    child:   Text("88".tr,style: const TextStyle(color: MyColors.lightpurple,),
                  ),
                ),
                )],
            ),
          ),
        ),
      ),

    );
  }
}
