import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class MyLocaleController extends GetxController {
  String currentLang = 'en'; // default language is English

  void changeLang() {
    if (currentLang == 'en') {
      currentLang = 'ar';
    } else if (currentLang == 'ar') {
      currentLang = 'en';
    }

    Locale locale = Locale(currentLang);
    Get.updateLocale(locale);
  }
}
