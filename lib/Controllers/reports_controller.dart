import 'dart:convert';
import 'dart:developer';

import 'package:get/get.dart';
import 'package:get/get_connect/http/src/response/response.dart';
import 'package:medicine_warehouse/Models/reports_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;



class ReportsController extends GetxController{
  var url = "http://127.0.0.1:8000/api/getMedicineSalesReport";
 var reports= <Reports>[].obs;
  ReportsModel? reportsModel;

   // Add this line

  dynamic getReports() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  try {
  final http.Response response = await http.get(Uri.parse(url),
  headers: {"Authorization": "Bearer ${prefs.getString('token')}"});
  Map<String, dynamic> data = jsonDecode(response.body);
  if (response.statusCode == 200) {
  print(reports.toString());

  log(response.body);
  reportsModel = ReportsModel.fromJson(data); // Update this line
  reports.value = reportsModel!.reports!;
  return reports;
  } else if (response.statusCode != 200) {
  Get.snackbar('53'.tr, '54'.tr,
  snackPosition: SnackPosition.BOTTOM);
  print('Post request failed with status: ${response.statusCode}.');
  }
  } catch (e) {
  print('Error occurred: $e');
  }
  }
  }






