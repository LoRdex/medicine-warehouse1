import 'dart:convert';
import 'package:medicine_warehouse/Controllers/medicine_controller.dart';
import 'package:medicine_warehouse/Controllers/orders_controller.dart';
import 'package:medicine_warehouse/Controllers/reports_controller.dart';
import 'package:medicine_warehouse/Models/login_model.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:medicine_warehouse/Screens/home_screen.dart';
import 'package:medicine_warehouse/Screens/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
MedicineController medicineController = Get.find();
OrdersController ordersController = Get.find();
ReportsController reportsController = Get.find();
class LoginController extends GetxController {
  RxBool passwordVisibility = true.obs;
  var url = 'http://127.0.0.1:8000/api/login';
  var url1 = 'http://127.0.0.1:8000/api/logout';

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController passwordController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  Map<String, dynamic> data = {};

  void setPasswordVisibility() {
    passwordVisibility.value = !passwordVisibility.value;
  }

  String? passwordValidator(String? value) {
    if (value!.isEmpty) {
      return "14".tr;
    } else if (value.length < 8) {
      return '16'.tr;
    }
    return null;
  }

  String? emailOrPhoneValidator(String? value) {
    if (value == null || value.isEmpty) {
      return "13".tr;
    } else if (value.contains('@')) {
      if (!value.contains('.')) {
        return '22'.tr;
      }
    } else {
      if (!value.startsWith('09') || value.length != 10) {
        return '15'.tr;
      }
    }
    return null;
  }


  dynamic login(String email, String password) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      final http.Response response = await http.post(Uri.parse(url),
          body: {"loginame": email, "password": password});
      data = jsonDecode(response.body);
      if (response.statusCode == 200) {
        medicineController.getMedicines();
        ordersController.getOrders();
        reportsController.getReports();
        LoginModel loginModel = LoginModel.fromjson(data);
        Get.offAll(HomeScreen());
        Get.snackbar('51'.tr, '52'.tr,
            snackPosition: SnackPosition.BOTTOM);

        await prefs.setString('token', '${loginModel.token}');
        print(" cachced token : ${prefs.getString('token')}");
        return loginModel;
      } else if (response.statusCode != 200) {
        Get.snackbar('', '',
            snackPosition: SnackPosition.BOTTOM);
        print('Post request failed with status: ${response.statusCode}.');
      }
    } catch (e) {
      print('Error occurred: $e');
    }
  }
  dynamic logout ()
  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      final http.Response response = await http.post(Uri.parse(url1),
          headers: {"Authorization": "Bearer ${prefs.getString('token')}"});
          if(response.statusCode == 200)
            {
              await prefs.remove('token');
              Get.snackbar('55'.tr, '56'.tr,snackPosition: SnackPosition.BOTTOM);
              Get.offAll(const LoginScreen());
            }
          else if (response.statusCode != 200) {
            Get.snackbar('53'.tr, '54'.tr,
                snackPosition: SnackPosition.BOTTOM);
            print('Post request failed with status: ${response.statusCode}.');
          }
    }
    catch (e) {
      print('Error occurred: $e');
    }
  }

}
