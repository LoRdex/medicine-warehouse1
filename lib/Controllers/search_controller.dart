import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:medicine_warehouse/Models/search_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchControl extends GetxController {
  GlobalKey<FormState> searchKey = GlobalKey<FormState>();
  TextEditingController searchTextController = TextEditingController();
  var url = 'http://127.0.0.1:8000/api/search';
  var results = <SearchModel>[].obs;

  String? searchValidator(String? value) {
    if (value!.isEmpty) {
      return "81".tr;
    }
    return null;
  }

  dynamic search(String keyword) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      final http.Response response = await http.post(Uri.parse(url),
          headers: {"Authorization": "Bearer ${prefs.getString('token')}"},
          body: {"keyword": keyword});
      List<dynamic> data = jsonDecode(response.body);
      if (response.statusCode == 200) {
        print('get request successful!');
        results.value = data.map((item) => SearchModel.fromJson(item)).toList();
        Get.snackbar('73'.tr, '74'.tr,
            snackPosition: SnackPosition.BOTTOM);
        print(results.toString());
        return results;
      } else if (response.statusCode != 200) {
        Get.snackbar('75'.tr, '76'.tr,
            snackPosition: SnackPosition.BOTTOM);
        print('Post request failed with status: ${response.statusCode}.');
      }
    } catch (e) {
      print('Error occurred: $e');
    }
  }
}
