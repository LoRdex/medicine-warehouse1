import 'dart:convert';
import 'dart:developer';
import 'package:get/get.dart';
import 'package:medicine_warehouse/Models/order_details_model.dart';
import 'package:medicine_warehouse/Models/orders_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class OrdersController extends GetxController {
  var orders = <OrdersModel>[].obs;
  var details = <OrderDetails>[].obs;
  List<String> status = ['Preparation', 'Sent', 'Received'];
  List<String> paymentStatus = ['Paid', 'UnPaid'];
  RxString selectedStatus = 'Preparation'.obs;
  RxString selectedPaymentStatus = 'UnPaid'.obs;
  var getOrdersUrl = "http://127.0.0.1:8000/api/getOrders";

  void updateStatus(String newStatus) {
    selectedStatus.value = newStatus;
    update();
  }

  void updatePaymentStatus(String newPaymentStatus) {
    selectedPaymentStatus.value = newPaymentStatus;
    update();
  }

  void editPaymentStatus(String? paymentStatus, int? id) async {
    var paymentUrl = "http://127.0.0.1:8000/api/updatePaymentStatus/$id";

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      http.Response response = await http.put(Uri.parse(paymentUrl), headers: {
        "Authorization": "Bearer ${prefs.getString('token')}"
      },
          body:{"paymentStatus": paymentStatus});
      if(response.statusCode==200)
        {
          Get.snackbar("82".tr, "83".tr,snackPosition: SnackPosition.BOTTOM);
        }
      else {          Get.snackbar("53".tr, "54".tr,snackPosition: SnackPosition.BOTTOM);
      }
    }
    catch (e) {
      log('Error occurred: $e');
    }
  }
  void editStatus( String? status, int? id) async {
    var statusUrl = "http://127.0.0.1:8000/api/updateStatus/$id";

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      http.Response response = await http.put(Uri.parse(statusUrl), headers: {
        "Authorization": "Bearer ${prefs.getString('token')}"
      },
          body:{"status": status});
      if(response.statusCode==200)
      {
        Get.snackbar("82".tr, "83".tr,snackPosition: SnackPosition.BOTTOM);
      }
      else {          Get.snackbar("Failed", "Error Occur",snackPosition: SnackPosition.BOTTOM);
      }
    }
    catch (e) {
      log('Error occurred: $e');
    }
  }
  dynamic getOrders() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      final http.Response response = await http.get(Uri.parse(getOrdersUrl),
          headers: {"Authorization": "Bearer ${prefs.getString('token')}"});
      List<dynamic> data = jsonDecode(response.body);
      if (response.statusCode == 200) {
        print('get request successful!');
        orders.value = data.map((item) => OrdersModel.fromJson(item)).toList();
        return orders;
      } else if (response.statusCode != 200) {
        Get.snackbar('Failed !', 'Please Retry',
            snackPosition: SnackPosition.BOTTOM);
        print('Post request failed with status: ${response.statusCode}.');
      }
    } catch (e) {
      print('Error occurred: $e');
    }
  }


  dynamic getOrderDetails(int? id) async {
    var getOrderDetailsUrl = "http://127.0.0.1:8000/api/getSingleOrder/$id";
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      final http.Response response = await http.get(Uri.parse(getOrderDetailsUrl),
          headers: {"Authorization": "Bearer ${prefs.getString('token')}"});
      List<dynamic> data = jsonDecode(response.body);
      if (response.statusCode == 200) {
        print('get request successful!');
        details.value = data.map((item) => OrderDetails.fromJson(item)).toList();
        return details;
      } else if (response.statusCode != 200) {
        Get.snackbar('Failed !', 'Please Retry',
            snackPosition: SnackPosition.BOTTOM);
        print('Post request failed with status: ${response.statusCode}.');
      }
    } catch (e) {
      print('Error occurred: $e');
    }
  }




}


