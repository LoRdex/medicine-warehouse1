import 'dart:convert';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:medicine_warehouse/Models/medicine_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
class MedicineController extends GetxController {
  var url = 'http://127.0.0.1:8000/api/getMyMedicines';
  var medicines = <MedicineModel>[].obs;
  @override
  void onInit() {
    super.onInit();
    getMedicines();
  }
  dynamic getMedicines() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      final http.Response response = await http.get(Uri.parse(url),
          headers: {"Authorization": "Bearer ${prefs.getString('token')}"});
      List<dynamic> data = jsonDecode(response.body);
      if (response.statusCode == 200) {
        print('get request successful!');
         medicines.value = data.map((item) => MedicineModel.fromJson(item)).toList();
        Get.snackbar('67'.tr, '52'.tr,
            snackPosition: SnackPosition.BOTTOM);
        print(medicines.toString());
        return medicines;
      } else if (response.statusCode != 200) {
        Get.snackbar('53'.tr, '54'.tr,
            snackPosition: SnackPosition.BOTTOM);
        print('Post request failed with status: ${response.statusCode}.');
      }
    } catch (e) {
      print('Error occurred: $e');
    }
  }




}
