import 'package:get/get.dart';

class NavigationController extends GetxController {
  var currentTabIndex = 0.obs;
  var currentNavIndex = 0.obs;

  void changeTabIndex(int index) {
    currentTabIndex.value = index;
  }

  void changeNavIndex(int index) {
    currentNavIndex.value = index;
  }
}
