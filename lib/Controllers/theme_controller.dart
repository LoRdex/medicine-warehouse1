import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:medicine_warehouse/Components/colors.dart';

class ThemeController extends GetxController {
  var isDarkTheme = false.obs;

  void switchTheme() {
    isDarkTheme.value = !isDarkTheme.value;
  }

  ThemeData get themeData {
    if (isDarkTheme.value) {
      return ThemeData.dark().copyWith();
    } else {
      return ThemeData.light().copyWith(
      );
    }
  }
}
