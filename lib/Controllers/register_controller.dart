import 'dart:convert';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:medicine_warehouse/Models/register_model.dart';
import 'package:medicine_warehouse/Screens/home_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';


class RegisterController extends GetxController {
  RxBool passwordVisibility = true.obs;
  var url = 'http://127.0.0.1:8000/api/Warehouse-register';
  GlobalKey<FormState> formKeyReg = GlobalKey<FormState>();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController nameController = TextEditingController();


  Map<String, dynamic> data = {};

  void setPasswordVisibility() {
    passwordVisibility.value = !passwordVisibility.value;
  }

  String? passwordValidator(String? value) {
    if (value!.isEmpty) {
      return "14".tr;
    } else if (value.length < 8) {
      return '16'.tr;
    }
    return null;
  }

  String? emailValidator(String? value) {
    if (value!.isEmpty) {
      return "19".tr;
    } else if (!value.contains('@')) {
      return '15'.tr;
    }
    return null;
  }

  String? phoneValidator(String? value) {
    if (value == null || value.isEmpty) {
      return "18".tr;
    } else if (!value.contains('09')) {
      return '21'.tr;
    } else if (value.contains('09') && value.length < 10) {
      return "24".tr;
    }
    return null;
  }

  String? nameValidator(String? value) {
    if (value!.isEmpty) {
      return "17".tr;
    }
    return null;
  }

  dynamic register(String email, String password, String name, String phone) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      final http.Response response = await http.post(Uri.parse(url), body: {
        "email": email,
        "password": password,
        "phone": phone,
        "name": name
      });
      data = jsonDecode(response.body);
      //log(response.body);
      if (response.statusCode == 200) {
        RegisterModel registerModel = RegisterModel.fromjson(data);
        Get.snackbar('57'.tr, '52'.tr,
            snackPosition: SnackPosition.BOTTOM);
        await prefs.setString('token', '${registerModel.token}');
        Get.offAll(HomeScreen());
        return registerModel;
      } else if (response.statusCode != 200) {
        Get.snackbar('53'.tr, '54'.tr,
            snackPosition: SnackPosition.BOTTOM);
        print('Post request failed with status: ${response.statusCode}.');
      }
    } catch (e) {
      print('Error occurred: $e');
    }
  }
}
