
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:dio/dio.dart';

class AddController extends GetxController {


  Map<String, dynamic> data = {};
  GlobalKey<FormState> addFormKey = GlobalKey<FormState>();
  TextEditingController scNameController = TextEditingController();
  TextEditingController trNameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController quantityController = TextEditingController();
  TextEditingController categoryController = TextEditingController();
  TextEditingController manufactureController = TextEditingController();
  TextEditingController expDateController = TextEditingController();
  List<String> categories = ['39'.tr, '40'.tr, '41'.tr, '42'.tr];
  RxInt selectedCategoryIndex = RxInt(0);


  String? scNameValidator(String? value) {
    if (value!.isEmpty) {
      return "60".tr;
    }
    return null;
  }

  String? trNameValidator(String? value) {
    if (value!.isEmpty) {
      return "61".tr;
    }    return null;

  }

  String? priceValidator(String? value) {
    if (value!.isEmpty) {
      return "62".tr;
    }
    if (value.isNum == false) {
      return "63".tr;
    }    return null;

  }

  String? quantityValidator(String? value) {
    if (value!.isEmpty) {
      return "64".tr;
    }
    if (value.isNum == false) {
      return "63".tr;
    }
    return null;

  }

  String? manufactureValidator(String? value) {
    if (value!.isEmpty) {
      return "65".tr;
    }    return null;

  }

  // int? value categoryValidator(int? value) {
  //   if (value == null || value == 0) {
  //     return "Select a Medicine Category";
  //   }
  //}
  String? expDateValidator(String? value) {
    if (value!.isEmpty) {
      return "66".tr;
    }    return null;

  }

  void updateCategory(int index) {
    selectedCategoryIndex.value = index;
    categoryController.text = categories[index];
    update();
  }

  Future selectExpiryDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2099),
    );
    if (picked != null) {
      expDateController.text = DateFormat('yyyy-MM-dd').format(picked);
    }
  }

  // dynamic add(String scName, String trName, String manufacture, String category,
  //     String expDate, String price, String quantity) async {
  //   final SharedPreferences prefs = await SharedPreferences.getInstance();
  //   print(prefs.getString('token'));
  //   try {
  //     final http.Response response = await http.post(Uri.parse(url), body: {
  //       "scName": scName,
  //       "trName": trName,
  //       "category_id": category,
  //       "manufacturer": manufacture,
  //       "quantity": quantity,
  //       "expDate": expDate,
  //       "price": price
  //     }, headers: {
  //       "Authorization": "Bearer ${prefs.getString('token')}",
  //
  //     });
  //     if (response.statusCode == 200) {
  //       log('Post request successful!');
  //       Get.snackbar('58'.tr, '59'.tr,
  //           snackPosition: SnackPosition.BOTTOM);
  //     } else if (response.statusCode != 200) {
  //       Get.snackbar('53'.tr, '54'.tr,
  //           snackPosition: SnackPosition.BOTTOM);
  //       log('Post request failed with status: ${response.statusCode}.');
  //     }
  //   }
  //   catch (e, s) {
  //     if (e is ClientException) {
  //       log('ClientException occurred: ${e.message}');
  //       log('Stack trace: $s');
  //     } else {
  //       log('Error occurred: $e');
  //       log('Stack trace: $s');
  //     }
  //   }
  // }
  // dynamic add(String scName, String trName, String manufacture, String category,
  //     String expDate, String price, String quantity) async {
  //   final SharedPreferences prefs = await SharedPreferences.getInstance();
  //   print(prefs.getString('token'));
  //   try {
  //     Dio dio = new Dio();
  //     final response = await dio.post(
  //       url,
  //       data: {
  //         "scName": scName,
  //         "trName": trName,
  //         "category_id": category,
  //         "manufacturer": manufacture,
  //         "quantity": quantity,
  //         "expDate": expDate,
  //         "price": price
  //       },
  //       options: Options(
  //         headers: {
  //           "Authorization": "Bearer ${prefs.getString('token')}",
  //         },
  //       ),
  //     );
  //     if (response.statusCode == 200) {
  //       log('Post request successful!');
  //       Get.snackbar('58'.tr, '59'.tr,
  //           snackPosition: SnackPosition.BOTTOM);
  //     } else {
  //       Get.snackbar('53'.tr, '54'.tr,
  //           snackPosition: SnackPosition.BOTTOM);
  //       log('Post request failed with status: ${response.statusCode}.');
  //     }
  //   } catch (e) {
  //     log('Error occurred: $e');
  //   }
  // }
  void add(String scName, String trName, String manufacture, String category,
     String expDate, String price, String quantity) async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      final http.Response response = await http.post(Uri.parse("http://127.0.0.1:8000/api/createMedicines"),body: {
             "scName": scName,
             "trName": trName,
             "category_id": category,
             "manufacturer": manufacture,
             "quantity": quantity,
             "expDate": expDate,
             "price": price },
       headers:  {  "Authorization": "Bearer ${prefs.getString('token')}",});
      if (response.statusCode == 200) {
               log('Post request successful!');
               Get.snackbar('58'.tr, '59'.tr,
                   snackPosition: SnackPosition.BOTTOM);
             } else if (response.statusCode != 200) {
               Get.snackbar('53'.tr, '54'.tr,
                   snackPosition: SnackPosition.BOTTOM);
               log('Post request failed with status: ${response.statusCode}.');
             }
    }
   catch(e){
      print(e);
   }
    }
    
    
    
  }


