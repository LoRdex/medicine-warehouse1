import 'package:flutter/material.dart';
class MyColors {
  static const Color caramel = Color(0xffffd59a);
  static const Color gold_amber = Color(0xffc19552);
  static const Color amber_rose = Color(0xffeb8b6e);
  static const Color gognac = Color(0xff9a463d);
  static const Color honey = Color(0xffeba937);
  static const Color lightpurple = Color(0xffe0d8eb);
  static const Color lightpurple2 = Color(0xffe8def8);
  static const Color lightpurple3 = Color(0xffe9e3f1);
  static const Color purple = Color(0xff6750a4);
  static const Color midpurple = Color(0xff745eab);
  static const Color midpurple2 = Color(0xff8b79b9);
}