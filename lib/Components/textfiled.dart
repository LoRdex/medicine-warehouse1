import 'package:flutter/material.dart';
import 'package:medicine_warehouse/Models/login_model.dart';


class CustomTextFormField extends StatelessWidget {

  final String? Function(String? value)? validator;
   //double width=600.0;
  final String labelText;
  final InputBorder border;
  final IconData? preIcon;
  final Widget? suffixIcon;
  final TextInputType keyType;
  final TextEditingController controller;
  final bool obscureText;
  void Function()? onTap;

   CustomTextFormField(
      {super.key,
        this.validator,
        required this.labelText,
        this.border=const UnderlineInputBorder(),
        this.preIcon,
        this.suffixIcon,
        this.keyType=TextInputType.text,
        required this.controller,
        this.obscureText=false,
      this.onTap})
   ;

  @override
  Widget build(BuildContext context) {
    return
      SizedBox(width: MediaQuery.of(context).size.width/2.5,
        child: TextFormField(
         validator: validator,
         obscureText: obscureText,
         controller: controller,
         decoration:InputDecoration (
             border: border,
             labelText: labelText,
             suffixIcon: suffixIcon,
             prefixIcon: Icon(preIcon)),
         keyboardType: keyType,
           onTap:onTap,
        ),
      );
  }
}
