class MedicineModel {
  String? scName;
  String? trName;
  String? image;
  String? category;
  String? manufacture;
  int? quantity;
  int? price;
  String? expDate;

  MedicineModel(
      this.scName,
      this.trName,
      this.category,
      this.manufacture,
      this.quantity,
      this.price,
      this.expDate,
      this.image);

  factory MedicineModel.fromJson(Map<String, dynamic> data) {
    return MedicineModel(
        data["scName"],
        data["trName"],
        data["category"]["catName"],
        data["manufacturer"],
        data["quantity"],
        data["price"],
        data["expDate"],
        data["category"]["image"]);
  }

  @override
  String toString() {
    // TODO: implement toString
    return "scintefic name = $scName , trname = $trName ,  , manfuc =$manufacture  , quantity = $quantity , price =$price , exp = $expDate, CATEGORY = $category , image = $image";
  }
}
