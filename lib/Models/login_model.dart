import 'package:shared_preferences/shared_preferences.dart';

class LoginModel {
  int roleId;
  String name;
  String phone;
  String email;
  String? message;

  // String tokenType;
     String token;

  LoginModel(this.roleId, this.name, this.phone, this.email, this.message,this.token);

  factory LoginModel.fromjson(dynamic data) {
    return LoginModel(
        data["0"]["user"]["role_id"],
        data["0"]["user"]["name"],
        data["0"]["user"]["phone"],
        data["0"]["user"]["email"],
        data["message"],
        data["0"]["access_token"]);
  }

  @override
  String toString() {
    // TODO: implement toString
    return 'email = $email  ,  phone = $phone , name = $name message = $message  login token = $token';
  }
}
