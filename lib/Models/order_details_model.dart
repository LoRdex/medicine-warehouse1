class Medicine {
  String? scName;
  int? quantity;


  Medicine(this.scName, this.quantity);

  factory Medicine.fromJson(Map<String, dynamic> data) {
    return Medicine(data['scName'], data['quantity']);
  }
}

class OrderDetails {
  String? status;
  String? payStatus;
  int? totalPrice;
  List<Medicine>? medicines;

  OrderDetails(this.status, this.payStatus, this.medicines,this.totalPrice);

  factory OrderDetails.fromJson(Map<String, dynamic> data) {
    var medicinesFromJson = data['medicines'] as List;
    List<Medicine> medicinesList = medicinesFromJson.map((i) => Medicine.fromJson(i)).toList();

    return OrderDetails(data['status'], data['paymentStatus'], medicinesList,data['totalPrice']);
  }

  @override
  String toString() {
    // TODO: implement toString
    return "status = $status, payment : $payStatus, medicines = ${medicines.toString()}";
  }
}
