import 'package:shared_preferences/shared_preferences.dart';

class RegisterModel {
  int? roleId;
  String? name;
  String? phone;
  String? email;
  String? message;
  String? token;

  RegisterModel(this.roleId, this.name, this.phone, this.email, this.message,
      this.token); // String tokenType;

  factory RegisterModel.fromjson

  (
  Map<String, dynamic> json ) {
  var userJson = json['0']['user'];
  var message = json['message'];
  var accessToken = json['0']['access_token'];
  return RegisterModel(
  json["role_id"],
  json["name"],
  json["phone"],
  json["email"],
  message,
  accessToken);
  }
  @override
  String toString() {
  // TODO: implement toString
  return 'email = $email  ,  phone = $phone , name = $name message = $message  token = $token';
  }
}
