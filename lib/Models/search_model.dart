class SearchModel {
  String? scName;
  String? trName;
  String? category;
  String? manufacture;
  int? quantity;
  int? price;
  String? expDate;
  String? image;


  SearchModel(
      this.scName,
      this.trName,
      this.category,
      this.manufacture,
      this.quantity,
      this.price,
      this.expDate,
      this.image);



  factory SearchModel.fromJson(Map<String, dynamic> data) {
  return SearchModel(
  data["scName"],
  data["trName"],
  data["category"]["catName"],
  data["manufacturer"],
  data["quantity"],
  data["price"],
  data["expDate"],
  data["category"]["image"]);
  }

  @override
  String toString() {
  // TODO: implement toString
  return "scintefic name = $scName , trname = $trName ,  , manfuc =$manufacture  , quantity = $quantity , price =$price , exp = $expDate, CATEGORY = $category";

  }


}




// {
// "id": 1,
// "scName": "Paracetamol",
// "trName": "Cetamol",
// "category_id": 2,
// "manufacturer": "ASIA",
// "quantity": 120,
// "expDate": "2024-04-22",
// "price": 2000,
// "user_id": 1,
// "created_at": "2023-12-13 13:20:02",
// "updated_at": "2023-12-13 13:20:02",
// "category": "pills"
// },