class OrdersModel {
int? id;
String? status;
String? paymentStatus;

OrdersModel(this.id, this.status, this.paymentStatus);

factory OrdersModel.fromJson(Map <String,dynamic> data)
{
return OrdersModel(data["id"], data["status"], data["paymentStatus"]);
}
}
