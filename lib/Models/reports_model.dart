class Reports
{
  String? trName;
  String? totalQuantity;
  int? totalPrice;

  Reports(this.trName, this.totalQuantity, this.totalPrice);
  factory Reports.fromJson(Map<String,dynamic>data)
  {
    return Reports(data["trName"], data["total_quantity"], data["total_price"]);

  }
}
class ReportsModel
{
List<Reports>? reports;
int? totalSales;
String? message;

ReportsModel(this.reports, this.totalSales,this.message);
factory ReportsModel.fromJson(Map<String,dynamic>data)
{
  var reportsFromJson = data["report"] as List;
  List<Reports> reportList = reportsFromJson.map((i) => Reports.fromJson(i)).toList();
  return ReportsModel(reportList, data["totalSales"], data["message"]);
}
@override
String toString() {
  // TODO: implement toString
  return 'total sales = $totalSales';
}
}

